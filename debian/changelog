libscalar-defer-perl (0.23-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * debian/copyright: replace tabs with spaces / remove trailing
    whitespace.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 12 Dec 2022 20:50:43 +0000

libscalar-defer-perl (0.23-2.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Mon, 04 Jan 2021 17:08:25 +0100

libscalar-defer-perl (0.23-2) unstable; urgency=medium

  * Team upload

  [ Ansgar Burchardt ]
  * Update my email address.
  * debian/control: Convert Vcs-* fields to Git.

  [ Fabrizio Regalli ]
  * Bump to 3.9.2 Standard-Version.
  * Add myself to Uploaders and Copyright.
  * Switch d/compat to 8.
  * Build-Depends: switch to debhelper (>= 8).
  * Bump to 3.0 quilt format.
  * Fixed lintian copyright-refers-to-symlink-license message.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Fabrizio Regalli from Uploaders. Thanks for your work!
  * Remove Jaldhar H. Vyas from Uploaders. Thanks for your work!
  * Remove Jonathan Yu from Uploaders. Thanks for your work!
  * Remove Rene Mayorga from Uploaders. Thanks for your work!
  * Remove AGOSTINI Yves from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Florian Schlichting ]
  * Add no-dot-in-inc.patch and spelling.patch
  * Bump dh compat to level 11
  * Declare compliance with Debian Policy 4.1.5

 -- Florian Schlichting <fsfs@debian.org>  Sun, 08 Jul 2018 21:47:48 +0200

libscalar-defer-perl (0.23-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
  * Standards-Version 3.8.4 (drop perl version dep)
  * Use new short debhelper rules format
  * Add myself to Uploaders and Copyright
  * Rewrite control description
  * Update copyright to new DEP5 format

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ gregor herrmann ]
  * debian/control: require debhelper 7.2.13 because of Module::AutoInstall.

 -- Jonathan Yu <jawnsy@cpan.org>  Wed, 17 Feb 2010 22:00:38 -0500

libscalar-defer-perl (0.20-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

  [ Rene Mayorga ]
  * debian/control: update my email address.

  [ Ansgar Burchardt ]
  * New upstream release.
    + debian/copyright: Update years of copyright.
  * debian/copyright: List individual contributors as copyright holders,
    not the team.  The list of contributors was taken from the changelog.
  * Add myself to Uploaders.

 -- Ansgar Burchardt <ansgar@43-1.org>  Sun, 15 Feb 2009 00:07:58 +0100

libscalar-defer-perl (0.18-1) unstable; urgency=low

  [ Rene Mayorga ]
  * New upstream release
  * Add Module name to long description
  * Add /me to uploaders

  [ gregor herrmann ]
  * Add build dependency on libio-capture-perl to activate an additional test.

 -- Rene Mayorga <rmayorga@debian.org.sv>  Sat, 09 Aug 2008 21:08:38 -0600

libscalar-defer-perl (0.16-1) unstable; urgency=low

  * New upstream release.
  * Add /me to Uploaders.

 -- gregor herrmann <gregoa@debian.org>  Thu, 03 Jul 2008 00:02:01 +0200

libscalar-defer-perl (0.15-1) unstable; urgency=low

  * New upstream release
  * Standards-Version: 3.8.0 (no changes)
  * Refresh rules with use debhelper 7: changes in compat, and control
  * Remove useless README from package
  * Put copyright format to proposal rev 102, with inc/ copyright

 -- AGOSTINI Yves <agostini@univ-metz.fr>  Sat, 28 Jun 2008 16:58:48 +0200

libscalar-defer-perl (0.14-1) unstable; urgency=low

  * Initial Release (Closes: #476409)

 -- AGOSTINI Yves <agostini@univ-metz.fr>  Wed, 16 Apr 2008 09:40:10 +0200
